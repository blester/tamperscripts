// ==UserScript==
// @name       Easyur
// @namespace  http://bretlester.com
// @version    0.1
// @description  Makes imgur suck less (shows full-size images upon mouse-over of thumbnail)
// @match      http://imgur.com/*
// @copyright  2012+, You
// ==/UserScript==


var postElements = 0;

checkforNewElements();

function checkforNewElements() {
    var n = document.getElementsByClassName('post');
    if(n.length > postElements) {
        enableBehavior();
    }
    postElements = n.length;
    window.setTimeout(checkforNewElements, 10);
}

function enableBehavior() {
    var postElements = document.getElementsByClassName('post');

    for(var i=0; i < postElements.length; i++) {(function() {
        var p = postElements[i];
        var over = false;
        if(!p.__sucksless) {
            p.__sucksless = true;

            p.onmouseover = function() {
                over = true;
                var thumbSrc = this.getElementsByTagName('img')[0].src;
                var bigsrc = thumbSrc.substring(0,thumbSrc.lastIndexOf('b'))+thumbSrc.substring(thumbSrc.lastIndexOf('b')+1);

                window.setTimeout(function() {
                    if(!over) { return }
                    bigversion(bigsrc);
                }, 500);
            };

            p.onmouseout = function(e) {
                over = false;
                if(hasClass(e.relatedTarget, 'bretlester-nosuck', 'bretlester-nosuck-img')) {
                    return;
                }
                killZoomers();
            };
        }
    })()}
}

function getClasses(el) {
    var raw = el.getAttribute('class');
    raw = raw && raw.split(' ');
    var out = [];
    if(!raw) { return out }
    var c;
    for(var i=0; i < raw.length; i++) {
        c = raw[i];
        c = (c||'').trim();
        if(c) { out.push(c) }
    }
    return c;
}

function hasClass(el) {
    var classes;
    for(var i=1; i < arguments.length; i++) {
        classes = getClasses(el);
        if(classes.indexOf(arguments[i]) == -1) { return false }
    }
    return true;
}

function killZoomers() {
    var nosucks = document.getElementsByClassName('bretlester-nosuck');
    if(nosucks && nosucks.length) {
        for(var i=0; i < nosucks.length; i++) {
            document.body.removeChild(nosucks[i]);
        }
    }
}

function bigversion(src) {

    killZoomers();

    var padding = 20, border = 2;
    var ctr = document.createElement('div');
    ctr.setAttribute('class', 'bretlester-nosuck');
    ctr.style.border = border+'px solid #fff';
    ctr.style.zIndex = 9999;
    ctr.style.position = 'absolute';
    ctr.style.top = '0px';
    ctr.style.left = '0px';

    var img = document.createElement('img');
    img.setAttribute('class', 'bretlester-nosuck-img');
    img.style.verticalAlign = 'middle';
    img.src = src;
    ctr.appendChild(img);

    document.body.appendChild(ctr);

    trySize();
    function trySize() {
        if(img.complete) {
            sizeDown();
            return;
        }
        window.setTimeout(trySize, 1);
    }

    function sizeDown() {
        var maxw = window.innerWidth - padding*2 - border*2;
        var maxh = window.innerHeight - padding*2 - border*2;
        var w = img.width;
        var h = img.height;

        if(img.width > maxw) {
            console.log('resized by width');
            h = img.height = img.height*(maxw/img.width);
            w = img.width = maxw;
        }
        if(img.height > maxh) {
            console.log('resized by height');
            w = img.width = img.width*(maxh/img.height);
            h = img.height = maxh;
        }

        var left = (window.innerWidth/2 - (w/2 + border));
        var top = (window.innerHeight/2 - (h/2 + border));

        //console.log(left+'x'+top);

        ctr.style.left = (left)+'px';
        ctr.style.top = (window.scrollY + top)+'px';

        img.onmouseout = function() {
            killZoomers();
        };
    }
}

